package com.example.demo;

import com.example.demo.model.Communication;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableBatchProcessing
@EnableScheduling
@EnableRetry
public class DemoApplication {

	public static void main(String[] args) {
		try {
			SpringApplication.run(DemoApplication.class, args);
			Injector injector = Guice.createInjector(new BasicModule());
			Communication comms = injector.getInstance(Communication.class);
			comms.sendMessage("Yagyank was here");
		} catch (Exception e ){
			System.out.println(e);
		}

	}

}
