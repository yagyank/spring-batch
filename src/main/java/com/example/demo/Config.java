package com.example.demo;

import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.launch.NoSuchJobException;
import org.springframework.batch.core.launch.NoSuchJobExecutionException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@EnableBatchProcessing
@EnableScheduling
public class Config {

  private JobRunner jobRunner;

  public Config(JobRunner jobRunner){
    this.jobRunner = jobRunner;
  }

  @Scheduled(cron = "0 0/1 * 1/1 * *")
//  @Scheduled(cron = "* * * * * *")
  public void scheduleJob() throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException, NoSuchJobExecutionException, NoSuchJobException {
    System.out.println("job triggered");
    jobRunner.runJob();
  }
}