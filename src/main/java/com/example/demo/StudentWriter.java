package com.example.demo;

import com.example.demo.model.Student;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StudentWriter implements ItemWriter<Student> {
  @Override
  public void write(List<? extends Student> items) throws Exception {
    System.out.println(items);
    System.out.println("as");
  }
}
