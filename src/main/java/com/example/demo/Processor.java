package com.example.demo;

import com.example.demo.model.Student;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class Processor implements ItemProcessor<Student, Student> {

  @Autowired
  private JdbcTemplate jdbcTemplate;


  @Override
  public Student process(Student item) throws Exception {

//    int a =jdbcTemplate.queryForObject("SELECT id FROM val LIMIT 1", Integer.class);
//    if(item.getId() == a) {
//      throw new RuntimeException("Exception");
//    }
    return item;
  }
}
