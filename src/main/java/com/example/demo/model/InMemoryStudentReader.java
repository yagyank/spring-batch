package com.example.demo.model;

import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
public class InMemoryStudentReader implements ItemReader<Student> {

  @Autowired
  private JdbcTemplate jdbcTemplate;

  private int nextStudentIndex;
  private List<Student> studentData;

  public InMemoryStudentReader() {
    initialize();
  }

  private void initialize() {
    Student s1 = new Student(1, "ABC");
    Student s2 = new Student(2, "DEF");
    Student s3 = new Student(3, "GHI");

    studentData = Collections.unmodifiableList(Arrays.asList(s1, s2,s3));
    nextStudentIndex = 0;
  }

  @Override
  @Retryable(include = { RuntimeException.class }, maxAttempts = 1000, backoff = @Backoff(delay = 0))
  public Student read() {
    Student nextStudent = null;

    if (nextStudentIndex < studentData.size()) {
      int a =jdbcTemplate.queryForObject("SELECT id FROM val LIMIT 1", Integer.class);
      if(a == 2) {
        throw new RuntimeException("Exception");
      }
      nextStudent = studentData.get(nextStudentIndex);
      nextStudentIndex++;
    } else {
      nextStudentIndex = 0;
    }

    return nextStudent;
  }
}