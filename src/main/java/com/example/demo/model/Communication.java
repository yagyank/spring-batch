package com.example.demo.model;

import com.google.inject.Inject;

public class Communication {

  private Communicator communicator;

  @Inject
  public Communication(Communicator communicator) {
    this.communicator = communicator;
  }

  public Communication(Boolean keepRecords) {
    if (keepRecords) {
      System.out.println("Message logging enabled");
    }
  }

  public boolean sendMessage(String message) {
    return communicator.sendMessage(message);
  }

}