package com.example.demo;

import com.example.demo.model.InMemoryStudentReader;
import com.example.demo.model.Student;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.DuplicateJobException;
import org.springframework.batch.core.configuration.JobFactory;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.retry.annotation.Retryable;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

@Configuration
@EnableRetry
public class JobConfig {
  @Bean
  @Qualifier("students")
  public Job job1(JobBuilderFactory jobBuilderFactory,
                  StepBuilderFactory stepBuilderFactory,
                  ItemReader<Student> reader,
                  ItemProcessor<Student, Student> processor,
                  ItemWriter<Student> writer) {

    Step step = stepBuilderFactory.get("xxxx")
        .<Student, Student>chunk(1)
        .reader(reader)
        .processor(processor)
        .writer(writer)
        .faultTolerant().retryLimit(500000).retry(RuntimeException.class)
        .build();

    return jobBuilderFactory
        .get("readStudents")
        .start(step)
        .build();
  }

  @Bean
//  @Retryable(include = {RuntimeException.class}, maxAttempts = 1000, backoff = @Backoff(delay = 0))
  public ItemReader<Student> reader() {
    return new InMemoryStudentReader();
  }

  @Bean
  public JobOperator jobOperator(final JobLauncher jobLauncher, final JobRepository jobRepository,
                                 JobRegistry jobRegistry, final JobExplorer jobExplorer, Job job) throws DuplicateJobException {

    jobRegistry.register(new JobFactory() {
      @Override
      public Job createJob() {
        return job;
      }

      @Override
      public String getJobName() {
        return job.getName();
      }
    });
    final SimpleJobOperator jobOperator = new SimpleJobOperator();
    jobOperator.setJobLauncher(jobLauncher);
    jobOperator.setJobRepository(jobRepository);
    jobOperator.setJobRegistry(jobRegistry);
    jobOperator.setJobExplorer(jobExplorer);
    return jobOperator;
  }

  @Bean
  public RetryTemplate retryTemplate() {
    RetryTemplate retryTemplate = new RetryTemplate();

    FixedBackOffPolicy fixedBackOffPolicy = new FixedBackOffPolicy();
    fixedBackOffPolicy.setBackOffPeriod(1l);
    retryTemplate.setBackOffPolicy(fixedBackOffPolicy);

    SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy();
    retryPolicy.setMaxAttempts(2000);
    retryTemplate.setRetryPolicy(retryPolicy);

    return retryTemplate;
  }
}
