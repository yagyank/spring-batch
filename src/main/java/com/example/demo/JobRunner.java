package com.example.demo;

import org.springframework.batch.core.*;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.batch.core.launch.NoSuchJobException;
import org.springframework.batch.core.launch.NoSuchJobExecutionException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class JobRunner {

  @Autowired
  @Qualifier("students")
  private Job job;
  @Autowired
  private JobLauncher simpleJobLauncher;
  @Autowired
  private JobOperator jobOperator;
  @Autowired
  private JdbcTemplate jdbcTemplate;
  @Autowired
  private JobExplorer jobExplorer;
  @Autowired
  private RetryTemplate retryTemplate;

//  @Autowired
//  public JobRunner(Job job, JobLauncher jobLauncher, JobOperator jobOperator, JdbcTemplate jdbcTemplate, JobExplorer jobExplorer) {
//    this.simpleJobLauncher = jobLauncher;
//    this.job = job;
//    this.jobOperator = jobOperator;
//    this.jdbcTemplate = jdbcTemplate;
//    this.jobExplorer = jobExplorer;
//  }


  public void runJob() throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException, NoSuchJobExecutionException, NoSuchJobException {
//    Long id = jdbcTemplate.queryForObject("SELECT job_execution_id FROM batch_job_execution order by start_time DESC LIMIT 1", Long.class);
//    String status = jdbcTemplate.queryForObject("SELECT status FROM batch_job_execution order by start_time DESC LIMIT 1", String.class);
//    if (status.equals("FAILED")) {
//      final Long restartId = jobOperator.restart(id);
//    } else if (status.equals("STARTED")) {
//      simpleJobLauncher.run(job, jobExplorer.getJobExecution(id).getJobParameters());
//    }
//    else {
//    retryTemplate.execute((RetryCallback<Void, RuntimeException>) context -> {
//      JobParameters jobParameters =
//          new JobParametersBuilder()
//              .addString("time", LocalDate.now().toString()).toJobParameters();
//      simpleJobLauncher.run(job, jobParameters);
//    }
    JobParameters jobParameters =
        new JobParametersBuilder()
            .addString("time", LocalDate.now().toString()).toJobParameters();
    simpleJobLauncher.run(job, jobParameters);
  }
}